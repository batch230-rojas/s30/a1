// Test Objects
db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);


// 1. Result of using MongoDB Aggregation to count the total number of fruits on sale.
// Insert your query below... 

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$count: "fruitsOnSale"}
	])


// 2. Result of using MongoDB Aggregation to count the total number of fruits with stock more than 20.
// Insert your query below... 

db.fruits.aggregate([
		{$match: {stock: {$gte:20}}},
		{$count: "enoughStock"}
	])

// 3. Result of using MongoDB Aggregation to get the average price of fruits onSale per supplier
// Insert your query below... 

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
		]
	)


// 4. Result of using MongoDB Aggregation to get the highest price of a fruit per supplier
// Insert your query below... 

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
		]
	)


// 5. Result of using MongoDB Aggregation to to get the lowest price of a fruit per supplier.
// Insert your query below... 

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
		]
	)



// Number 4 and 5,
// Additional requirement:
// It should match with onSale: true, before grouping
